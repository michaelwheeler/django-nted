from django.test import TestCase

from xml.dom import minidom
from nted import models


class TestNted(TestCase):

    def setUp(self):
        pass

    def test_instructor_str(self):
        instructor = models.NtedInstructor(
            first_name="Some",
            last_name="Guy",
            phone="4445556666",
            email="sg@example.com"
        )
        self.assertEqual(instructor.__str__(), "Guy, Some")

    def test_instructor_res_element(self):
        instructor = models.NtedInstructor(
            first_name="Some",
            last_name="Guy",
            phone="4445556666",
            email="sg@example.com"
        )
        doc = minidom.Document()
        element = instructor.get_res_element(doc)
        self.assertEqual(element.tagName, 'instructorpoc')
        self.assertEqual(element.attributes.length, 4)

    def tearDown(self):
        pass
