#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nted

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = nted.__version__

setup(
    name='django-nted',
    version=version,
    packages=[
        'nted',
    ],
    install_requires=[
        'django>=1.8.0'
    ],
    tests_require=[
        'tox>=1.7.0'
    ]
)
