from __future__ import unicode_literals
from xml.dom import minidom
from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class NtedTrainingProvider(models.Model):
    """
    """
    abbreviation = models.CharField(max_length=25, unique=True)
    institution = models.CharField(max_length=255)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['abbreviation']

    def __str__(self):
        return self.abbreviation

    def get_res_element(self, doc):
        element = doc.createElement('trainingprovider')
        element.setAttribute('tpid', self.abbreviation)
        element.setAttribute('tpphone', self.phone)
        element.setAttribute('tpemail', self.email)
        return element


@python_2_unicode_compatible
class NtedAudienceType(models.Model):
    """
    """
    abbreviation = models.CharField(max_length=5, unique=True)
    discipline = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['abbreviation']

    def __str__(self):
        return self.discipline


@python_2_unicode_compatible
class NtedTrainingMethod(models.Model):
    """
    """
    code = models.CharField(max_length=1, unique=True)
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['code']

    def __str__(self):
        return "[{}] {}".format(self.code, self.name)


@python_2_unicode_compatible
class NtedGovernmentLevel(models.Model):
    """
    """
    abbreviation = models.CharField(max_length=5, unique=True)
    level = models.CharField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['abbreviation']

    def __str__(self):
        return self.level


@python_2_unicode_compatible
class NtedCourse(models.Model):
    """
    """
    name = models.CharField(max_length=255)
    # TODO: Make sure number field is sutible for the new course numbers
    number = models.CharField(max_length=50, unique=True)
    # provider = models.ForeignKey('NtedTrainingProvider')
    description = models.TextField(null=True, blank=True)
    length = models.DecimalField(max_digits=6, decimal_places=2)
    # method =
    # level =
    continuing_education_units = models.DecimalField(max_digits=6,
                                                     decimal_places=2)
    objectives = models.TextField(null=True, blank=True)
    prerequisites = models.TextField(null=True, blank=True)
    # target_audience = models.ManyToManyField('NtedAudienceType')
    # training_certificates = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True
        ordering = ['number']

    def __str__(self):
        return self.number


@python_2_unicode_compatible
class NtedInstructor(models.Model):
    """
    """
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=100)

    class Meta:
        abstract = True
        ordering = ['last_name', 'first_name']

    def __str__(self):
        return "{}, {}".format(self.last_name, self.first_name)

    def get_res_element(self, doc):
        element = doc.createElement('instructorpoc')
        element.setAttribute('instlastname', self.last_name)
        element.setAttribute('instfirstname', self.first_name)
        element.setAttribute('instphone', self.phone)
        element.setAttribute('instemail', self.email)
        return element
